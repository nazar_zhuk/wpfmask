﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace WpfTestMask.MaskTextBox
{
    public class MaskedTextBox : TextBox
    {
        private string _mask;
        public string Mask
        {
            get
            {
                return _mask;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    _mask = value;
                    OnEventInit();
                }
            }
        }
        public String ProcessMask { get; set; }
        public String CompleteMask { get; set; }
        public Boolean IsCompleteMask
        {
            get
            {
                return (Boolean)this.GetValue(Complete); ;
            }
            set
            {
                SetValue(Complete, value);
            }
        }
        public static DependencyProperty Complete = DependencyProperty.Register("IsCompleteMask", typeof(Boolean), typeof(MaskedTextBox), new PropertyMetadata(false));

        private int _index;
        private Indexer _indexer;
        public MaskedTextBox()
            : base()
        {
        }
        private void OnEventInit()
        {
            _indexer = new Indexer(Mask);
            PreviewTextInput += TextBox_TextInput;
            KeyDown += Window_KeyDown;
            PreviewKeyDown += Window_KeyDown;
            KeyUp += GetChar_KeyUp;
            MaxLength = Mask.Length;
            Text = Mask;
        }
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Back || e.Key == Key.Delete)
            {
                e.Handled = true;
            }
        }
        private void GetChar_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Back || e.Key == Key.Delete)
            {
                e.Handled = true;
                StringBuilder sb = new StringBuilder(Text);
                _index = sb[_indexer.Current()] == '*' ? _indexer.Previous() : _indexer.Current();
                sb[_index] = '*';
                Text = sb.ToString();
                CaretIndex = _index + 1;
                IsCompleteMask = false;
            }
        }
        private void TextBox_TextInput(object sender, TextCompositionEventArgs e)
        {
            if (Regex.IsMatch(e.Text, ProcessMask))
            {
                StringBuilder sb = new StringBuilder(Text);
                _index = sb[_indexer.Current()] != '*' ? _indexer.Next() : _indexer.Current();
                if (sb[_index] == '*')
                {
                    sb[_index] = e.Text[0];
                    Text = sb.ToString();
                    CaretIndex = _index + 1;
                    IsCompleteMask = Regex.IsMatch(Text, CompleteMask);
                }
            }
        }
    }
}
