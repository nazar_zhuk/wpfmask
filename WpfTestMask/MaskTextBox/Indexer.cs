﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;


namespace WpfTestMask.MaskTextBox
{
    public class Indexer
    {
        List<int> Indexes;
        private int _index = -1;
        public Indexer(string mask)
        {
            Regex rgx = new Regex(@"[*]");
            Indexes = rgx.Matches(mask).Cast<Match>().Select(x => x.Index).ToList();
        }
        public int Current()
        {
            if (_index == -1) _index = 0;
            return Indexes[_index];
        }
        public int Next()
        {
            if (_index < Indexes.Count - 1)
            {
                return Indexes[++_index];
            }

            return _index == Indexes.Count - 1 ? Indexes[_index] : 0;
        }
        public int Previous()
        {
            if (_index == -1)
            {
                return Indexes[++_index];
            }

            if (_index > 0)
            {
                return Indexes[--_index];
            }

            return _index == 0 ? Indexes[_index] : 0;
        }

    }
}
